package com.example.twitterlogindemo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class LoginToTwitter extends Activity {

	protected static final String CALLBACK_URL_KEY = "CALLBACK_URL_KEY";
	private ProgressBar progressbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_to_twitter);

		Intent intent = getIntent();
		String mUrl = intent
				.getStringExtra(MainActivity.AUTHENTICATION_URL_KEY);

		WebView webView = (WebView) findViewById(R.id.webViewLoginToTwitter);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.setWebViewClient(new LoginToTwitterWebViewClient());
		progressbar = (ProgressBar) findViewById(R.id.progressBar1);
		webView.loadUrl(mUrl);
	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	private class LoginToTwitterWebViewClient extends WebViewClient {
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			
			progressbar.setVisibility(ProgressBar.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			
			if (progressbar.getVisibility() == ProgressBar.VISIBLE) {
				
				progressbar.setVisibility(ProgressBar.GONE);
			}
			super.onPageFinished(view, url);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			
			if (url.startsWith(MainActivity.TWITTER_CALLBACK_URL)) {
				
				Intent intent = new Intent();
				intent.putExtra(CALLBACK_URL_KEY, url);
				setResult(Activity.RESULT_OK, intent);
				finish();
			}
			return false;
		}
	}
}